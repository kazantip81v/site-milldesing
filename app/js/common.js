$(function() {

	$(".toggle-menu").click(function () {
			$(this).toggleClass("on");
			$(".menu").slideToggle();
	});
	
	$('nav a').click(function (event) {

		$('nav li').removeClass('active');
		$(this).parent().toggleClass('active');
		
		$(".toggle-menu").removeClass("on");
		$(".menu").slideToggle();
		
	});

	$('a[href^="#"]').on('click', function(event) {

		let target = $( $(this).attr('href') );

		if( target.length ) {
				event.preventDefault();
				$('html, body').animate({
						scrollTop: target.offset().top-45
				}, 1000);
		}
	});

	let elem = $('#arrow-up');
		$(window).scroll(function(){
				elem[($(this).scrollTop() > 680 ? 'show': 'hide')](100);       
		});


	
	$("#header .button").animated("wobble");

	$("#home h2, #home h3").animated("bounceInDown");
	$("#home p").animated("swing");

	$("#about-us h2").animated("shake");
	$("#about-us").waypoint(function () {
		$("#about-us p").each(function (index) {
			var flag = $(this);
			setTimeout(function () {
				flag.addClass("on");
			}, 300*index);
		});
	}, {
		offset: "55%"
	});

	$("#about-us").waypoint(function () {
		$(".about-item").each(function (index) {
			var flag = $(this);
			setTimeout(function () {
				flag.addClass("on").animated("rollIn");
			}, 300*index);
		});
	}, {
		offset: "55%"
	});

	$("#portfolio h2").animated("fadeInRight");
	$("#portfolio p").animated("fadeInLeft");
	$("#portfolio").waypoint(function () {
		$(".portfolio-item").each(function (index) {
			var flag = $(this);
			setTimeout(function () {
				flag.animated("flipInY");
			}, 200*index);
		});
	}, {
		offset: "55%"
	});

	$("#price h2, #price .header p").animated("lightSpeedIn");
	$("#price").waypoint(function () {
		$(".price-item").each(function (index) {
			var flag = $(this);
			setTimeout(function () {
				flag.addClass("zoom");
			}, 500*index);
		});
	}, {
		offset: "55%"
	});

	$("#contacts form").animated("fadeInRightBig");

	// owl-carousel
	$(".slider").owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		navText: "",
		autoplay: true,
		smartSpeed: 500,
		fluidSpeed: 500,
		autoplaySpeed: 700,
		navSpeed: 500,
		dodsSpead: 500,
		dragEndSpeed: 500
	});

	
	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};

	//E-mail Ajax Send
	//Documentation & Example: https://github.com/agragregra/uniMail
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Thank you!");
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});

});
